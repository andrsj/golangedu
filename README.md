# Golang education

Project describe what is the goroutine and shows how and when we need to use some patterns of usage

* Goroutine are extremely lightweight compared to OS threads.
* Stack size is very small of 2kb as opposed to 8MB of stack of OS threads.
* Context switching is very cheap as it happens in user space, goroutines have very less state to be stored.
* Hundreds of thousands of goroutines can be created on single machine.

---

## M:N Scheduler

* The Go scheduler is part of the Go runtime. It is known as M:N scheduler
* Go scheduler uses OS threads + schedule goroutines for execution.
* Goroutines runs in the context of OS threads.
* Go runtime create number of worker OS threads, equal to GOMAXPROCS.
* GOMAXPROCS - default value is number of processors on machine.
* Go scheduler distributes runnable goroutines over multiple worker OS threads.
* At any time, N goroutines could be scheduled on M OS threads
that runs on at most GOMAXPROCS numbers of processors.

## Scheduler

Asynchronous Preemption

* As of Go 1.14, Go scheduler implements asynchronous preemption.
* This prevents long running Goroutines from hogging onto CPU, that could block other Goroutines.
* The asynchronous preemption is triggered based on a time condition.
When a goroutine is running for more than 10ms, Go will try to preempt it.

---

### What happens in general when synchronous system call are made?

* synchronous system calls wait for I/O operation to be completed.
* OS thread is moved out of the CPU to waiting queue for I/O to complete.
* Synchronous system call reduces parallelism.
* When Goroutine makes synchronous system call, Go scheduler bring a new OS thread from thread pool.
* Moves the logical processor P to new thread.
* Goroutine which made the system call will still be attached to old thread.
* Other Goroutines in LRQ are scheduled for execution on new OS thread.
* Once system call returns, Goroutine is moved back to run queue on logical processor P and old thread is put to sleep.

### What happens in general when asynchronous system call are made?

* File descriptor is set to non-blocking mode
* If file descriptor is not ready, for I/O operation, system call does not block, but returns an error.
* Asynchronous IO increases the application complexity.
* Setup event loops using callbacks functions.

---

### Other pages

* [Channel](./example/channel/README.md)
* [Context](./example/context/README.md)
* [Simple example Crawl](./example/crawl/README.md)
* [Mutex](./example/mtx/README.md)
* [Patterns](./example/patterns/README.md)

---

## Netpoller

* Netpoller is a mechanism to convert asynchronous system call to blocking system call.
* When a goroutine makes a asynchronous system call, and file descriptor is not ready, goroutine is parked at netpoller os thread.
* netpoller uses interface provided by OS to do polling on file descriptors
  * kqueue (MacOS)
  * epoll (Linux)
  * iocp(Windows)
* Netpoller gets notification from OS, when file descriptor is ready for I/O operation.
* Netpoller notifies goroutine to retry I/O operation.
* Complexity of managing asynchronous system call is moved from Application to Go runtime, which manages it efficiently.
* netpoller uses interface provided by OS to do polling on file descriptors and notifies the goroutine to try I/O operation when it ready.

### Work Stealing Rule

* If there is no goroutines in local run queue.
* Try to steal from other logical processors.
* If not found, check the global runnable queue for a G
* If not found, check netpoller.
* Work stealing helps in better distribution of goroutines across all logical processors.

---

```note
States of goroutine:

                <[Preempted]<
            /                   \
    [Runnable] - - - - - - - - > [Executing]
            \                  /
I/O or event \                / (I/O or event wait)
 completion   \              /
                 <[Waiting]<
```

---

```note
    [CORE]
       |
       |
       |
      [M] (OS Thread)
       |
       | - - [G1*] (goroutine)                | - - [G2*]
                   G - include stack, instruction pointer, etc
       |                                      |
      [P] (Processor)                        [P]
      P - logical processor which manages scheduling of goroutines
           \                                     \
            \                                     \
            [G2] (local run queue)                 [G3]
            [G3]        LRQ                        [G4]
            [G4]                                   [G1]
```

---
