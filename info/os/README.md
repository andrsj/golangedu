# OS pkg

```go
const path = "path/to/file"
stat, err := os.Stat(path)
if os.IsNotExist(err) {
    // ...
}

// . . .

stat.Name()     // Base name of the file
stat.Size()     // Length in bytes for regular files
stat.Mode()     // File mode bits
stat.ModTime()  // Last modification time
stat.IsDir()    // Abbreviation for Mode().IsDir()

// . . .

os.Rename(old_path, new_path)

// . . .

f, err := os.OpenFile(filename, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)

// . . .

os.MkdirAll(path, 0755)

// . . .

file := os.Create(path)
file.Close()

// . . .

os.Remove(path)

```
