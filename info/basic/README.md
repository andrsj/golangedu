# GO

___

## `if` construction

```go
if x := 100; x == 100 {
    fmt.Println(x)
}
// can't use var 'x' outside of the IF
```

___

## `switch`

```go
fmt.Print("Go runs on ")
switch os := runtime.GOOS; os {
case "darwin":
    fmt.Println("OS X.")
case "linux":
    fmt.Println("Linux.")
default:
    // freebsd, openbsd,
    // plan9, windows...
    fmt.Printf("%s.\n", os)
}
```

___

## Other
