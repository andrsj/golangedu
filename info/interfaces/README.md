# Interface

* Abstract Type
* Set of methods signatures

___

Type is compile time property:

```go
var a *Metal
a = &gold
a.Density()

// |
// |
// v

(*Metal)Density()
```

Interface:

```go
var a Dense
a = &gold
a.Density()

// |
// |
// v

// ?
```

Interface values:

* Dynamic type
* Dynamic value

```note
                       d
                type  [nil]
var d Dense
                value [nil]



                       d
                type  [*Metal]        Metal
var d Dense                         [mass:   ...]
                value [   - -]- ->  [volume: ...]
```

`d.Density()`

* Method call through an interface must use dynamic dispatch.
* Compiler would have generated code to obtain the address of the method from the type descriptor, then make an indirect call to that address.

```note
      d
type [*Metal] ---> Density()
```

* The receiver argument for the call is a copy of the interface's dynamic value.

```note
                      Metal
       d            [mass:   ...]
value [   - -]- ->  [volume: ...]

EQUAL TO:

d.Density()
    |
    |
    v
gold.Density()
```

* Interface enables us to encapsulate the logic within user defined data type.
* Interfaces are satisfied implicitly.
* User defined data types just need to possess the methods defined in interface to be considered an instance of interface.
* We are not locking us with abstraction at the start of a project.
* We can define interfaces as and when abstractions become apparent.
* This design lets us create new interfaces that are satisfied by existing concrete types, without changing the existing types.
* Interface definition and concrete type definition could
appear in any package without prearrangement.
* It makes it easier to abstract dependencies.
* Keep Interfaces simple and short.
Define interface when there are two or more concrete types that must be dealt with in a uniform way.
* Create smaller interfaces with fewer, simpler methods.

___

## io.Writer

```go
type Writer interface {
    Write(p []byte) (n int, err error)
}
```

* The `io.Writer` interface type is one of the most widely used interfaces.
* It provides an abstraction of all the types to which bytes can be written, which includes:
  * Files
  * Memory buffers
  * Network connections
  * HTTP clients

___

```go
type Reader interface {
    Read(p []byte) (n int, err error)
}

type Closer interface {
    Close() error
}
type ReadWriter interface {
    Reader
    Writer
}
type ReadWriteCloser interface {
    Reader
    Writer
    Closer
}
```

___

## Stringer

```go
type Stringer interface {
    String() string
}
```

* `Stringer` interface provides a way for types to control how their values are printed.
* The `fmt` package functions (`Println`, `Fprintln`, ...) checks if concrete type has string method, if it does, then they call string method of the type to format values.


___

* A type satisfies an interface if it implements all the methods the interface defines.
* Interface wraps concrete type.
* Only methods defined by interface are revealed even if concrete type implements other methods.

___

```note
[type UDPConn struct]
                        - - -> [type Conn interface]
[type TCPConn struct]

# BUT #

      [conn]
        |
        |
        v
 [Dynamic value]
  |           |
  |           |
  v           v
[TCPConn]    [UDPConn]
  |             |
  v             x
.CloseWrite()
```

___

## Type assertion

`v := x.(T)`

* Checks interface values's dynamic type is identical to
concrete type typename, returns dynamic value.
* If check fails, then the operation panics.

`v, ok := x.(T)`

* The second return value is boolean.
* If type assertion is successful,
  * v == dynamic value, ok == true
* If type assertion is fails,
  * v == zero value of type, ok == false
* No run-time panic occurs in this case.

> * Type assertion is used to get concrete value from interface
value by specifying the explicit type.
> * Type assertion is useful to apply distinguished operation of
the type.
