package main

import (
	"fmt"

	"gitlab.com/andrsj/golangedu/example/channel"
	"gitlab.com/andrsj/golangedu/example/context"
	"gitlab.com/andrsj/golangedu/example/crawl"
	"gitlab.com/andrsj/golangedu/example/mtx"
	"gitlab.com/andrsj/golangedu/example/patterns/builder"
	p_channel "gitlab.com/andrsj/golangedu/example/patterns/channel"
	"gitlab.com/andrsj/golangedu/example/patterns/solid"
	"gitlab.com/andrsj/golangedu/example/wg"

)

var mapFunctions = make(map[string]func())

func main() {
	mapFunctions["ContextCancel"] = context.ContextCancel
	mapFunctions["ContextTimeout"] = context.ContextTimeout
	mapFunctions["ContextDeadline"] = context.ContextDeadline
	mapFunctions["ContextWithValue"] = context.ContextValue

	mapFunctions["Closure"] = wg.ClosureExample

	mapFunctions["DirectedChan"] = channel.DirectedChan
	mapFunctions["ChannelOwner"] = channel.ChannelOwner
	mapFunctions["RaceCondition"] = channel.ClearRaceCondition
	mapFunctions["ChannelTimeout"] = channel.TimeoutByChannel

	mapFunctions["Once"] = mtx.OnceExample
	mapFunctions["Mutex"] = mtx.MutexExample
	mapFunctions["Signal"] = mtx.SignalExample
	mapFunctions["Broadcast"] = mtx.BroadcastExample

	mapFunctions["SequentialCrawl"] = crawl.RecursiveURLs
	mapFunctions["ConcurrentCrawl"] = crawl.RecursiveURLsConcurrent

	mapFunctions["Pipeline"] = p_channel.PipelineExample
	mapFunctions["FanInOutExample"] = p_channel.FanInOutExample
	mapFunctions["CancelingExample"] = p_channel.CancelingExample

	mapFunctions["SPR"] = solid.SingleResponsibilityPrinciple
	mapFunctions["OPC"] = solid.OpenClosedPrinciple

	mapFunctions["Builder"] = builder.Builder
	mapFunctions["PartialBuilder"] = builder.PartialBuilder
	mapFunctions["FunctionBuilder"] = builder.FunctionBuilder
	mapFunctions["BuilderAsParameter"] = builder.BuilderAsParameter
	mapFunctions["GuruBuilder"] = builder.GuruBuilder

	nameFunction := "DirectedChan"
	runFunction(nameFunction)
}

func runFunction(name string) {
	fmt.Printf("\nRun function '%s'\n\n", name)
	function, ok := mapFunctions[name]
	if !ok {
		fmt.Println("Not found option")
	}
	function()
	fmt.Printf("\nEnd of '%s' function\n", name)
}
