package channel

import "fmt"

func generator(start, end int) <-chan int {
	fmt.Println("[generator]: Create channel [int] with data")
	out := make(chan int)

	fmt.Println("[generator]: Create and run goro")
	go func() {
		fmt.Println("[generator[goro]]: Looping from start to end")
		for i := start; i < end; i++ {
			fmt.Println("[generator[goro]]: Send value to chan:", i)
			out <- i
		}
		fmt.Println("[generator[goro]]: Closing channel")
		close(out)
	}()

	fmt.Println("[generator]: Returning channel")
	return out
}

func square(in <-chan int) <-chan int {
	fmt.Println("[square]: Create channel [int] with updated data")
	out := make(chan int)

	fmt.Println("[square]: Create and run goro")
	go func() {
		fmt.Println("[square[goro]]: Looping through channel")
		for n := range in {
			fmt.Println("[square[goro]]: Send updated value", n, "to", n*n)
			out <- n * n
		}
		fmt.Println("[square[goro]]: Closing channel")
		close(out)
	}()

	fmt.Println("[square]: Returning channel")
	return out
}

func PipelineExample() {

	// data_chan := generator(1, 20)
	// updated_data_chan := square(data_chan)
	// . . .

	fmt.Println("Running pipeline generator -> square")
	for n := range square(generator(1, 20)) {
		fmt.Println(n)
	}
}
