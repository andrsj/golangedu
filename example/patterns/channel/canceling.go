package channel

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func CancelingExample() {
	fmt.Println("Create channel for sending signal")
	done := make(chan struct{})

	fmt.Println("Create channel for generated data")
	in := generatorCancel(done, 1, 20)

	fmt.Println("Create channels for updating data")
	ch1 := squareNamedCancel(done, in, "First")
	ch2 := squareNamedCancel(done, in, "Second")

	go func() {
		fmt.Println("Merging updated data into one channel")
		for n := range mergeCancel(done, ch1, ch2) {
			fmt.Println(n)
		}
	}()

	time.Sleep(100 * time.Millisecond)
	done <- struct{}{}

	fmt.Println("Number of running goro:", runtime.NumGoroutine())

}

func generatorCancel(done <-chan struct{}, start, end int) <-chan int {
	fmt.Println("[generator]: Create channel [int] with data")
	out := make(chan int)

	fmt.Println("[generator]: Create and run goro")
	go func() {
		fmt.Println("[generator[goro]]: defer closing channel")
		defer close(out)

		fmt.Println("[generator[goro]]: Looping from start to end")
		for i := start; i < end; i++ {

			// Imitate hard work
			time.Sleep(10 * time.Millisecond)

			select {
			case out <- i:
				fmt.Println("[generator[goro]]: Send value to chan:", i)
			case <-done:
				return
			}
		}
	}()

	fmt.Println("[generator]: Returning channel")
	return out
}

func squareNamedCancel(done <-chan struct{}, in <-chan int, name string) <-chan int {
	fmt.Println("[square]: Create channel [int] with updated data")
	out := make(chan int)

	fmt.Println("[square]: Create and run goro")
	go func() {
		fmt.Println("[square[goro]]: Looping through channel")
		defer close(out)
		for n := range in {

			// Imitate hard work
			time.Sleep(10 * time.Millisecond)

			select {
			case out <- n * n:
				fmt.Printf("%s : %d\n", name, n)
				fmt.Println("[square[goro]]: Send updated value", n, "to", n*n)
			case <-done:
				return
			}
		}
	}()

	return out
}

func mergeCancel(done <-chan struct{}, cs ...<-chan int) <-chan int {
	fmt.Println("[merge]: Create channel for sync data")
	out := make(chan int)

	var wg sync.WaitGroup

	fmt.Println("[merge]: Create func for merge data to one channel")
	output := func(c <-chan int) {
		defer wg.Done()
		for n := range c {
			select {
			case out <- n:
				fmt.Println("[merge[output]]: Send value to channel")
			case <-done:
				return
			}
		}

	}
	wg.Add(len(cs))

	fmt.Println("[merge]: Looping thought channels")
	for _, c := range cs {
		fmt.Println("[merge]: Running func")
		go output(c)
	}
	go func() {
		wg.Wait()
		fmt.Println("[merge]: Closing channel")
		close(out)
	}()

	fmt.Println("[merge]: Returning result channel")
	return out
}
