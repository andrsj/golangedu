# Patterns of concurrent go code

## Pipeline

| Components | Description |
| ---------- | ----------- |
| Filter | It transforms or filters the data it receives through the channels it is associated with. A filter can have any number of input channels and any number of output channels |
| Channel | A connector that passes data from one filter to another |
| Producer | It is the data source that constantly creates new data |
| Receiver or consumer | It is the target of the data. It could be another file, a database, or a computer screen |

Example of code: [fan-in-out.go](./fan-in-out.go)

```note
                  square()
                 /        \
generator () -> |          | -> merge() -> print
                 \        /
                  square()
```

What is fan-out?

* Multiple goroutines are started to read data from the single channel.
* Distribute work amongst a group of workers goroutines to parallelize the CPU usage and the I/O usage.
* Helps computational intensive stage to run faster.

What is Fan-in?

* Process of combining multiple results into one channel.
* We create Merge goroutines, to read data from multiple input channels and send the data to a single output channel.

### Real Pipelines

* Real pipelines - Receiver Stages may only need a subset of values to make progress.
* A stage can exit early because an inbound value represents an error in an earlier stage.
* Receiver should not have to wait for the remaining values to arrive
* We want earlier stages to stop producing values that later
stages don't need.

---

* Pass a read-only ‘done’ channel to goroutine
* Close the channel, to send broadcast signal to all goroutine.
* On receiving the signal on done channel, Goroutines needs
to abandon their work and terminate.

---

## Fan-out, fan-in

Multiple functions can read from the same channel until that channel is closed; it's called fan-out.
This makes it possible to distribute work among a group of workers in order to parallelize CPU and I/O usage.

The function can read multiple inputs and run until they are all closed by multiplexing the input channels into a single channel that is closed when all inputs are closed. It's called fan-in.

```go
in := gen(2, 3)
// Share work sq for two goroutine, 
// which both read from in.
c1 := sq(in)
c2 := sq(in)

// Merging two channels
for n := range merge(c1, c2) {
    fmt.Println(n)
}
```

---

Fan-In is a messaging pattern used to create a funnel for work among performers.

```go
// Merge different channel into one
func Merge(cs ...<-chan int) <-chan int {
  var wg sync.WaitGroup

  out := make(chan int)

  // Run send goroutine 
  // for every cs channel 
  // send copy values from c into out 
  // until c is not closed, after call wg.Done.
  send := func(c <-chan int) {
    for n := range c {
      out <- n
    }
    wg.Done()
  }

  wg.Add(len(cs))
  for _, c := range cs {
    go send(c)
  }

  // Run goroutine for closing out 
  // when all send goroutine are done
  go func() {
    wg.Wait()
    close(out)
  }()
  return out
}
```

The `Merge` function converts the list of channels into a single channel by running a goroutine for each incoming channel that copies the values into a single outgoing channel.

Once all send goroutines have been run, a separate goroutine is started with a function waiting for all those running to complete in order to close the main channel.

---

## Canceling pipeline

Canceling usually realized by using channel like a receiver of signal with empty `struct{}{}`

Of course we need to describe different logic in pipelines in `select` statement

```go
select {
   case out <- i:
    // Do job
   case <-done:
    // Finish job
    return
}
```

And can send signal into `done` channel by one line of code: `done <- struct{}{}`

[Back to patterns](../README.md)

[Back to main page](../../../README.md)
