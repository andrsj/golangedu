package channel

import (
	"fmt"
	"sync"
)
func squareNamed(in <-chan int, name string) <-chan int {
	fmt.Println("[square]: Create channel [int] with updated data")
	out := make(chan int)

	fmt.Println("[square]: Create and run goro")
	go func() {
		fmt.Println("[square[goro]]: Looping through channel")
		for n := range in {
			fmt.Printf("%s : %d\n", name, n)
			fmt.Println("[square[goro]]: Send updated value", n, "to", n*n)
			out <- n * n
		}
		close(out)
	}()

	return out
}

func FanInOutExample() {
	fmt.Println("Create channel for generated data")
	in := generator(1, 20)

	fmt.Println("Create channels for updating data")
	ch1 := squareNamed(in, "First")
	ch2 := squareNamed(in, "Second")

	fmt.Println("Merging updated data into one channel")
	for n := range merge(ch1, ch2) {
		fmt.Println(n)
	}
}

func merge(cs ...<-chan int) <-chan int {
	fmt.Println("[merge]: Create channel for sync data")
	out := make(chan int)

	var wg sync.WaitGroup

	fmt.Println("[merge]: Create func for merge data to one channel")
	output := func(c <-chan int) {
		for n := range c {
			fmt.Println("[merge[output]]: Send value to channel")
			out <- n
		}
		wg.Done()
	}
	wg.Add(len(cs))

	fmt.Println("[merge]: Looping thought channels")
	for _, c := range cs {
		fmt.Println("[merge]: Running func")
		go output(c)
	}
	go func() {
		wg.Wait()
		fmt.Println("[merge]: Closing channel")
		close(out)
	}()

	fmt.Println("[merge]: Returning result channel")
	return out
}
