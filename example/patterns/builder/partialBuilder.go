package builder

import "fmt"

type Person struct {
	StreetAddress, Postcode, City string
	CompanyName, Position         string
	AnnualIncome                  int
}

type PersonBuilder struct {
	person *Person // needs to be initialized
}

func NewPersonBuilder() *PersonBuilder {
	fmt.Println("NewPersonBuilder set up")
	return &PersonBuilder{&Person{}}
}

func (it *PersonBuilder) Build() *Person {
	fmt.Println("Building the person")
	return it.person
}

func (it *PersonBuilder) Works() *PersonJobBuilder {
	fmt.Println("Setting work place [Job builder]")
	return &PersonJobBuilder{*it}
}

func (it *PersonBuilder) Lives() *PersonAddressBuilder {
	fmt.Println("Setting home [Address builder]")
	return &PersonAddressBuilder{*it}
}

type PersonJobBuilder struct {
	PersonBuilder
}

func (pjb *PersonJobBuilder) At(companyName string) *PersonJobBuilder {
	fmt.Println("[Job builder]: Setting company name")
	pjb.person.CompanyName = companyName
	return pjb
}

func (pjb *PersonJobBuilder) AsA(position string) *PersonJobBuilder {
	fmt.Println("[Job builder]: Setting position")
	pjb.person.Position = position
	return pjb
}

func (pjb *PersonJobBuilder) Earning(annualIncome int) *PersonJobBuilder {
	fmt.Println("[Job builder]: Setting annual income")
	pjb.person.AnnualIncome = annualIncome
	return pjb
}

type PersonAddressBuilder struct {
	PersonBuilder
}

func (it *PersonAddressBuilder) At(streetAddress string) *PersonAddressBuilder {
	fmt.Println("[Address builder]: Setting street address")
	it.person.StreetAddress = streetAddress
	return it
}

func (it *PersonAddressBuilder) In(city string) *PersonAddressBuilder {
	fmt.Println("[Address builder]: Setting city")
	it.person.City = city
	return it
}

func (it *PersonAddressBuilder) WithPostcode(postcode string) *PersonAddressBuilder {
	fmt.Println("[Address builder]: Setting postcode")
	it.person.Postcode = postcode
	return it
}

func PartialBuilder() {
	pb := NewPersonBuilder()
	pb.
		Lives().
		At("Shevchenka").
		In("Chervonograd").
		WithPostcode("80100").
		Works().
		At("Luxoft").
		AsA("Golang developer").
		Earning(1200)
	person := pb.Build()
	fmt.Printf("%#v\n", person)

	fmt.Println("\n\nStep by step:")
	pb = NewPersonBuilder()
	fmt.Printf("%#v\n", pb)

	pb.Lives().At("Street")
	fmt.Printf("%#v\n", pb.person)

	pb.Lives().In("City")
	fmt.Printf("%#v\n", pb.person)

	pb.Lives().WithPostcode("code")
	fmt.Printf("%#v\n", pb.person)

	pb.Works().At("Company")
	fmt.Printf("%#v\n", pb.person)

	pb.Works().AsA("Position")
	fmt.Printf("%#v\n", pb.person)

	pb.Works().Earning(10000)
	fmt.Printf("%#v\n", pb.person)

	person = pb.Build()
	fmt.Printf("%#v\n", person)
}
