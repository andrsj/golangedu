package builder

import "fmt"

type FPerson struct {
	name, position string
}

type personMod func(*FPerson)
type FPersonBuilder struct {
	actions []personMod
}

func (b *FPersonBuilder) Build() *FPerson {
	fmt.Println("[Build]: Setting zero Person")
	p := FPerson{}
	fmt.Println("[Build]: Reduce all functions to person")
	for _, a := range b.actions {
		a(&p)
	}
	return &p
}

func (b *FPersonBuilder) Called(name string) *FPersonBuilder {
	fmt.Println("[Builder]: Adding func for setting name")
	b.actions = append(b.actions, func(p *FPerson) {
		fmt.Println("[Builder[<func>]]: Setting name")
		p.name = name
	})
	return b
}

func (b *FPersonBuilder) WorksAsA(position string) *FPersonBuilder {
	fmt.Println("[Builder]: Adding func for setting position")
	b.actions = append(b.actions, func(p *FPerson) {
		fmt.Println("[Builder[<func>]]: Setting position")
		p.position = position
	})
	return b
}

func FunctionBuilder() {
	fmt.Println("Create builder and build person")
	b := FPersonBuilder{}
	p := b.Called("<Name>").WorksAsA("<Position>").Build()
	fmt.Printf("%#v\n", *p)
}
