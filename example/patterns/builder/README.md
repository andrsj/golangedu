# Builder

Builder is a creation design pattern that lets you construct complex objects step by step.
The pattern allows you to produce different types and representations of an object using the same construction code.
The Builder pattern suggests that you extract the object construction code out of its own class and move it to separate objects called builders.

Some of the construction steps might require different implementation when you need to build various representations of the product.
In this case, you can create several different builder classes that implement the same set of building steps, but in a different manner.
Then you can use these builders in the construction process to produce different kinds of objects.

___

When to use Builder?

* Some objects are simple and can be created in a single constructor call
* Other objects require a lot of ceremony to create
* Having a factory function ~~with 10 arguments~~ is not productive
* Instead, opt for piecewise (piece-by-piece) construction
* Builder provides an API for constructing an object step-by-step

> When piecewise object construction is complicated, provide an API for doing it

* A builder is a separate component used for building an object
* To make builder fluent, return the receiver — allows chaining
* Different facets of an object can be built with different builders working in tandem via a common struct

___

## Usage

```go
old_b := NewHtmlBuilder("ul")
old_b.AddChild("li", "hello")
old_b.AddChild("li", "world")
fmt.Println(old_b.String())
```

OR [with chain of methods]

```go
b := NewHtmlBuilder("ul")
b.AddChildFluent("li", "hello").AddChildFluent("li", "world")
fmt.Println(b.String())
```

Realizations:

```go

func (b *HtmlBuilder) AddChild(childName, childText string) {
    // . . .
}

// OR

func (b *HtmlBuilder) AddChildFluent(childName, childText string) *HtmlBuilder {
    // . . .
}
```

___

## Partial builder

```go
type PersonBuilder struct {
    person *Person // needs to be initialized
}

func NewPersonBuilder() *PersonBuilder {
    return &PersonBuilder{&Person{}}
}

func (pb *PersonBuilder) Build() *Person {
    return pb.person
}

type PersonJobBuilder struct {
    PersonBuilder
    // Person.company
    // Person.position
    // Person.salary
}

type PersonAddressBuilder struct {
    PersonBuilder
    // Person.address
    // Person.city
    // Person.postcode
}

func (it *PersonBuilder) Works() *PersonJobBuilder {}
func (pjb *PersonJobBuilder) At(companyName string) *PersonJobBuilder {}
func (pjb *PersonJobBuilder) AsA(position string) *PersonJobBuilder {}
func (pjb *PersonJobBuilder) Earning(annualIncome int) *PersonJobBuilder {}

func (it *PersonBuilder) Lives() *PersonAddressBuilder {}
func (it *PersonAddressBuilder) At(streetAddress string) *PersonAddressBuilder {}
func (it *PersonAddressBuilder) In(city string) *PersonAddressBuilder {}
func (it *PersonAddressBuilder) WithPostcode(postcode string) *PersonAddressBuilder {}
```

**USAGE**:

```go
pb := NewPersonBuilder()
pb.
    Lives().
        At(<Street>).
        In(<City>).
        WithPostcode(<postcode>).
    Works().
        At(<Company>).
        AsA(<Position>).
        Earning(<salary>)

person := pb.Build()
```

___

## Builder as a parameter

> For clients who uses this builder only without knowledge of real struct

```go
type build func(*EmailBuilder)

func SendEmail(action build) {  // user defined anon func
    builder := EmailBuilder{}
    action(&builder)
    email := builder.build()
    // . . .
}
```

**USAGE:**

```go
SendEmail(func(b *EmailBuilder) {
    // User doesn't know real struct
    b.From("<e-mail From>").
        To("<e-mail To>").
        Subject("<Subject>").
        Body("<Body of e-mail>")
})

```

___

## Function builder

```go
type Person struct {}

type personMod func(*Person)
type PersonBuilder struct {
    actions []personMod
}

func (b *PersonBuilder) Build() *Person {
    p := Person{}
    for _, a := range b.actions {
        a(&p)
    }
    return &p
}

func (b *PersonBuilder) Called(name string) *PersonBuilder {
    b.actions = append(b.actions, func(p *Person) {
        p.name = name
    })
    return b
}

// e t c
// usage: as usual builder in chain mode
```

___

## GURU

> Client -[setting Builder `Director.settingBuilder`]
>> Director -[using Builder `Director.buildProduct`]
>>> Builder -[creating Product `Builder.buildProduct`] `// actual realization`
>>>> setting Product [`Builder.setParameterA`]
>>>>
>>>> setting Product [`Builder.setParameterB`]
>>>>
>>>> setting Product [`Builder.setParameterC`]
>>>>
>>>> . . .
>>>>> Product

**Example [guruBuilder.go](./guruBuilder.go)**

[Back to patterns](../README.md)

[Back to main page](../../../README.md)
