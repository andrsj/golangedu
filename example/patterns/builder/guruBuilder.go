package builder

import "fmt"

type IBuilder interface {
	setWindowType()
	setDoorType()
	setNumFloor()
	getHouse() House
}

func getBuilder(builderType string) IBuilder {
	fmt.Println("[getBuilder]: returning Builder depends on input")
	if builderType == "normal" {
		return newNormalBuilder()
	}

	if builderType == "igloo" {
		return newIglooBuilder()
	}
	return nil
}

type NormalBuilder struct {
	windowType string
	doorType   string
	floor      int
}

func newNormalBuilder() *NormalBuilder {
	fmt.Println("[newNormalBuilder]: returning NormalBuilder")
	return &NormalBuilder{}
}

func (b *NormalBuilder) setWindowType() {
	fmt.Println("[NormalBuilder]: Setting Window")
	b.windowType = "Wooden Window"
}

func (b *NormalBuilder) setDoorType() {
	fmt.Println("[NormalBuilder]: Setting Door")
	b.doorType = "Wooden Door"
}

func (b *NormalBuilder) setNumFloor() {
	fmt.Println("[NormalBuilder]: Setting floor")
	b.floor = 2
}

func (b *NormalBuilder) getHouse() House {
	fmt.Println("[NormalBuilder]: Getting House")
	return House{
		doorType:   b.doorType,
		windowType: b.windowType,
		floor:      b.floor,
	}
}

type IglooBuilder struct {
	windowType string
	doorType   string
	floor      int
}

func newIglooBuilder() *IglooBuilder {
	fmt.Println("[newIglooBuilder]: returning IglooBuilder")
	return &IglooBuilder{}
}

func (b *IglooBuilder) setWindowType() {
	fmt.Println("[IglooBuilder]: Setting window")
	b.windowType = "Snow Window"
}

func (b *IglooBuilder) setDoorType() {
	fmt.Println("[IglooBuilder]: Setting Door")
	b.doorType = "Snow Door"
}

func (b *IglooBuilder) setNumFloor() {
	fmt.Println("[IglooBuilder]: Setting Floor")
	b.floor = 1
}

func (b *IglooBuilder) getHouse() House {
	fmt.Println("[IglooBuilder]: Getting House")
	return House{
		doorType:   b.doorType,
		windowType: b.windowType,
		floor:      b.floor,
	}
}

type House struct {
	windowType string
	doorType   string
	floor      int
}

type Director struct {
	builder IBuilder
}

func newDirector(b IBuilder) *Director {
	return &Director{
		builder: b,
	}
}

func (d *Director) setBuilder(b IBuilder) {
	fmt.Println("[Director]: Setting builder")
	d.builder = b
}

func (d *Director) buildHouse() House {
	fmt.Println("[Director]: Building House")
	d.builder.setDoorType()
	d.builder.setWindowType()
	d.builder.setNumFloor()
	return d.builder.getHouse()
}

func GuruBuilder() {
	fmt.Println("[]: Getting builders")
	normalBuilder := getBuilder("normal")
	iglooBuilder := getBuilder("igloo")

	fmt.Println("[]: Setting up director")
	director := newDirector(normalBuilder)

	fmt.Println("[]: Building by director")
	normalHouse := director.buildHouse()

	fmt.Println("[]: Output")
	fmt.Printf("Normal House Door Type: %s\n", normalHouse.doorType)
	fmt.Printf("Normal House Window Type: %s\n", normalHouse.windowType)
	fmt.Printf("Normal House Num Floor: %d\n", normalHouse.floor)

	fmt.Println("[]: Setting up director")
	director.setBuilder(iglooBuilder)

	fmt.Println("[]: Building by director")
	iglooHouse := director.buildHouse()

	fmt.Println("[]: Output")
	fmt.Printf("Igloo House Door Type: %s\n", iglooHouse.doorType)
	fmt.Printf("Igloo House Window Type: %s\n", iglooHouse.windowType)
	fmt.Printf("Igloo House Num Floor: %d\n", iglooHouse.floor)

}
