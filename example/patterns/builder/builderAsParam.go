package builder

import "fmt"

type email struct {
	from, to, subject, body string
}

type EmailBuilder struct {
	email email
}

func (b *EmailBuilder) build() *email {
	return &b.email
}

func (b *EmailBuilder) From(from string) *EmailBuilder {
	fmt.Println("[Builder]: Setting From")
	b.email.from = from
	return b
}

func (b *EmailBuilder) To(to string) *EmailBuilder {
	fmt.Println("[Builder]: Setting To")
	b.email.to = to
	return b
}

func (b *EmailBuilder) Subject(subject string) *EmailBuilder {
	fmt.Println("[Builder]: Subject")
	b.email.subject = subject
	return b
}

func (b *EmailBuilder) Body(body string) *EmailBuilder {
	fmt.Println("[Builder]: Body")
	b.email.body = body
	return b
}

func sendMailImpl(email *email) {
	fmt.Println("Sending e-mail . . .")
}

type build func(*EmailBuilder)

func SendEmail(action build) {
	fmt.Println("[SendEmail]: Create builder")
	builder := EmailBuilder{}

	fmt.Println("[SendEmail]: Use this builder with user func")
	action(&builder)

	sendMailImpl(&builder.email)
	sendMailImpl(builder.build())
}

func BuilderAsParameter() {
	SendEmail(func(b *EmailBuilder) {
		fmt.Println("User defined anon func with set up builder")
		b.From("<e-mail From>").
			To("<e-mail To>").
			Subject("<Subject>").
			Body("<Body of e-mail>")
	})
}
