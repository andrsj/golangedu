package solid

import "fmt"

// Combination of OCP and Specification pattern demo
func OpenClosedPrinciple() {
	fmt.Println("Setting up the values")
	apple := Product{"Apple", green, small}
	tree := Product{"Tree", green, large}
	house := Product{"House", blue, large}

	products := []Product{apple, tree, house}

	fmt.Println("Setting up Specification")
	greenSpec := ColorSpecification{green}
	largeSpec := SizeSpecification{large}
	largeGreenSpec := AndSpecification{largeSpec, greenSpec}

	fmt.Println("Setting up filters")
	// Good way
	f := Filter{}

	// Best way
	bf := BetterFilter{}

	// Good way
	fmt.Println("Printing result by good filter:")
	fmt.Print("Green products:\n")
	for _, v := range f.filterByColor(products, green) {
		fmt.Printf(" - %s is green\n", v.name)
	}

	// Best way
	fmt.Println("Printing result by best filter:")
	fmt.Print("Green products (new):\n")
	for _, v := range bf.Filter(products, greenSpec) {
		fmt.Printf(" - %s is green\n", v.name)
	}

	fmt.Print("Large blue items:\n")
	for _, v := range bf.Filter(products, largeGreenSpec) {
		fmt.Printf(" - %s is large and green\n", v.name)
	}

	greenItems1 := f.filterByColor(products, green)
	greenItems2 := bf.Filter(products, greenSpec)
	greenLargeItems1 := f.filterBySizeAndColor(products, large, green)
	greenLargeItems2 := bf.Filter(products, largeGreenSpec)
	largeItems1 := f.filterBySize(products, large)
	largeItems2 := bf.Filter(products, largeSpec)

	fmt.Println("Checking filtered slices by different filters")
	fmt.Println(compareSlicesOfProducts(greenItems1, greenItems2))
	fmt.Println(compareSlicesOfProducts(largeItems1, largeItems2))
	fmt.Println(compareSlicesOfProducts(greenLargeItems1, greenLargeItems2))
}

type Color int

const (
	red Color = iota
	green
	blue
)

type Size int

const (
	small Size = iota
	medium
	large
)

type Product struct {
	name  string
	color Color
	size  Size
}

type Filter struct {
}

func (f *Filter) filterByColor(products []Product, color Color) []*Product {
	result := make([]*Product, 0)

	for i, v := range products {
		if v.color == color {
			result = append(result, &products[i])
		}
	}

	return result
}

func (f *Filter) filterBySize(products []Product, size Size) []*Product {
	result := make([]*Product, 0)

	for i, v := range products {
		if v.size == size {
			result = append(result, &products[i])
		}
	}

	return result
}

func (f *Filter) filterBySizeAndColor(products []Product, size Size, color Color) []*Product {
	result := make([]*Product, 0)

	for i, v := range products {
		if v.size == size && v.color == color {
			result = append(result, &products[i])
		}
	}

	return result
}

// filterBySize, filterBySizeAndColor
type Specification interface {
	IsSatisfied(p *Product) bool
}

type ColorSpecification struct {
	color Color
}

func (spec ColorSpecification) IsSatisfied(p *Product) bool {
	return p.color == spec.color
}

type SizeSpecification struct {
	size Size
}

func (spec SizeSpecification) IsSatisfied(p *Product) bool {
	return p.size == spec.size
}

type AndSpecification struct {
	first, second Specification
}

func (spec AndSpecification) IsSatisfied(p *Product) bool {
	return spec.first.IsSatisfied(p) && spec.second.IsSatisfied(p)
}

type BetterFilter struct{}

func (f *BetterFilter) Filter(products []Product, spec Specification) []*Product {
	result := make([]*Product, 0)
	for i, v := range products {
		if spec.IsSatisfied(&v) {
			result = append(result, &products[i])
		}
	}
	return result
}

// Tools for comparing
func compareSlicesOfProducts(first, second []*Product) bool {
	if len(first) != len(second) {
		panic("Can't compare slices with different length")
	}

	for index := 0; index < len(first); index++ {
		if first[index] != second[index] {
			return false
		}
	}

	return true
}
