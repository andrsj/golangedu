# SOLID in GO

___

## Single Responsibility

| Author | Definition |
| ------ | ---------- |
| UNIX philosophy | "Do one thing and do it well" |
| Robert C. Martin [Uncle Bob] | "A class should have one, and only one, reason to change." |

> A type should only have one reason to change
>
> Separation of concerns — different types/packages handling different, independent tasks/problems

___

## Open Closed

| Author | Definition |
| ------ | ---------- |
| Robert C. Martin [Uncle Bob] | "A module should be open for extensions, but closed for modification." |
| Bertrand Meyer, Object-Oriented Software Construction | "Software entities should be open for extension, but closed for modification" |

> Types should be open for extension but closed for modification

Example in code: [`ocp.go`](./ocp.go)

```note
[Specification]   -> [Color & Size]
      |        \
      v         v
   [Color]    [Size]
```

___

## Liskov Substitution

| Author | Definition |
| ------ | ---------- |
| Robert C. Martin [Uncle Bob] | "Derived methods should expect no more and provide no less." |

> You should be able to substitute an embedding type in place of its embedded part

* In an object-oriented class-based language, the concept of the Liskov substitution principle is that a user of a base class should be able to function properly of all derived classes.
* In Golang there is no class-based inheritance.
* Instead, Golang provides a more powerful approach towards polymorphism via Interfaces and Struct Embedding.

Example:

We have problem that different Repositories can't be used in the same place due to different signature (in this case similar but `ctx` haven't used in `MemoryUserRepository`)

```go
type User struct {
   // . . .
}

type UserRepository interface {
    Create(ctx context.Context, user User) (*User, error)
    Update(ctx context.Context, user User) error
}

type DBUserRepository struct {
    db *gorm.DB
}

func (r *DBUserRepository) Create(ctx context.Context, user User) (*User, error) {
    err := r.db.WithContext(ctx).Create(&user).Error
    return &user, err
}

func (r *DBUserRepository) Update(ctx context.Context, user User) error {
    return r.db.WithContext(ctx).Save(&user).Error
}

type MemoryUserRepository struct {
    users map[uuid.UUID]User
}

func (r *MemoryUserRepository) Create(_ context.Context, user User) (*User, error) {
    if r.users == nil {
        r.users = map[uuid.UUID]User{}
    }
    user.ID = uuid.New()
    r.users[user.ID] = user
    
    return &user, nil
}

func (r *MemoryUserRepository) Update(_ context.Context, user User) error {
    if r.users == nil {
        r.users = map[uuid.UUID]User{}
    }
    r.users[user.ID] = user
    
    return nil
}
```

Fix: separate `interface` for different usage

```go
type User struct {
   // . . .
}

type UserRepository interface {
    Create(ctx context.Context, user User) (*User, error)
    Update(ctx context.Context, user User) error
}

type MySQLUserRepository struct {
    db *gorm.DB
}

type CassandraUserRepository struct {
    session *gocql.Session
}

type UserCache interface {
    Create(user User)
    Update(user User)
}

type MemoryUserCache struct {
    users map[uuid.UUID]User
}
```

___

## Interface Segregation

| Author | Definition |
| ------ | ---------- |
| Robert C. Martin [Uncle Bob] | "Many client specific interfaces are better than one general purpose interface" |

> Don’t put too much into an interface; split into separate interfaces
>
> YAGNI - You Ain’t Going to Need It

```go
type Reader interface {
    Read(p []byte) (n int, err error)
}

type Writer interface {
    Write(p []byte) (n int, err error)
}

type Closer interface {
    Close() error
}

type Seeker interface {
    Seek(offset int64, whence int) (int64, error)
}

type WriteCloser interface {
    Writer
    Closer
}

type ReadWriteCloser interface {
    Reader
    Writer
    Closer
}
```

___

## Dependency Inversion

| Author | Definition |
| ------ | ---------- |
| Robert C. Martin [Uncle Bob] | "High-level modules should not depend on low-level modules. Both should depend on abstractions. Abstraction should not depend upon details. Details should depend on abstractions." |

> High—level modules should not depend upon low-level ones; use abstractions

The Dependency Inversion Principle encourages you move the knowledge of the things your package depends on from compile time–in Go we see this with a reduction in the number of `import` statements used by a particular package–to run time.

* So, how to understand this, especially in the Go context? First, we should accept Abstraction as an OOP concept. We use such a concept to expose essential behaviors and hide details of their implementation.
* Second, what are high and low-level modules? High-level modules in the Go context are software components used on the top of the application, like code used for presentation.

[Back to patterns](../README.md)

[Back to main page](../../../README.md)
