package solid

import (
	"fmt"
	"net/url"
	"strings"
)

var entryCount = 0

const lineSeparator = "\n"

type Journal struct {
	entries []string
}

func (j *Journal) AddEntry(text string) int {
	entryCount++
	entry := fmt.Sprintf("%d: %s",
		entryCount,
		text)
	j.entries = append(j.entries, entry)
	return entryCount
}

// Wrong way
func (j *Journal) Save(filename string) {
	// Some implementation
	panic("Breaks Single Responsibility Principle")
}

// Wrong way
func (j *Journal) Load(filename string) {
	// Some implementation
	panic("Breaks Single Responsibility Principle")
}

// Wrong way
func (j *Journal) LoadFromWeb(url *url.URL) {
	// Some implementation
	panic("Breaks Single Responsibility Principle")
}

// Good way to use your object with other object
func PrintToConsole(j *Journal) {
	fmt.Println(strings.Join(j.entries, lineSeparator))
}

// Best way to use your object with full struct that can do
type Persistence struct {
	lineSeparator string
}

func (p *Persistence) PrintToConsole(j *Journal) {
	fmt.Println(strings.Join(j.entries, lineSeparator))
}

func SingleResponsibilityPrinciple() {

	fmt.Println("Create object which will be operated by other")
	j := Journal{}

	fmt.Println("Fill somehow this object")
	j.AddEntry("I cried today.")
	j.AddEntry("I ate a bug")

	fmt.Println("Printing by 'fmt' pkg")
	fmt.Println(strings.Join(j.entries, "\n"))

	// Separate function
	fmt.Println("Printing by 'PrintToConsole' func")
	PrintToConsole(&j)

	// Separate object [struct]
	fmt.Println("Configure 'Persistence' struct [obj]")
	p := Persistence{
		lineSeparator: lineSeparator,
	}
	fmt.Println("Printing by 'Persistence' struct [obj]")
	p.PrintToConsole(&j)
}
