# Patterns

* [Channels patterns](./channel/README.md)
* [SOLID](./solid/README.md)
* [Builder](./builder/README.md)

[Back to main page](../../README.md)
