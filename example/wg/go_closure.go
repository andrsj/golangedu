package wg

import (
	"fmt"
	"sync"
)

// ClosureExample show how to use WaitGroup
func ClosureExample() {
	fmt.Println("Create wg struct")
	var wg sync.WaitGroup

	fmt.Println("Create func 'incr'")
	incr := func(wg *sync.WaitGroup) {

		fmt.Println("[incr]: Create var i [int]")
		var i int

		fmt.Println("[incr]: Adding goro to wg")
		wg.Add(1)

		fmt.Println("[incr]: Run goro with incrementing i [int]")
		go func() {
			fmt.Println("[incr]: [goro]: Run 'Done' method in defer")
			defer wg.Done()

			fmt.Println("[incr]: [goro]: Incrementing")
			i++

			fmt.Println("[incr]: [goro]: Display value of i [int]")
			fmt.Printf("Value of i: %v\n", i)
		}()
		fmt.Println("[incr]: Return from function")
	}

	fmt.Println("Run incr(&wg)")
	incr(&wg)

	fmt.Println("wg.Wait()")
	wg.Wait()
	fmt.Println("Done...")
}
