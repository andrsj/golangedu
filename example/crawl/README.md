# Crawl package

This package shows the simple example how to do concurrent go code

## Meta

Code just search links in site and in sub-sites

`Crawl` gets 2 params:

* `url string`
* `depth string`

## Making concurrent

* The main idea to use all function like a goroutine
* Communicating between goroutine provided by `chan *result`
* The difference between simple realization and concurrent is x 6-10 times

[Back to main page](../../README.md)
