package crawl

import (
	"fmt"
	"time"
)

func RecursiveURLs() {
	fmt.Println("Create map of checked URLs")
	fetched = make(map[string]bool)

	fmt.Println("Set start time")
	now := time.Now()
	url := "http://andcloud.io"

	fmt.Println("Run crawl from root url:", url)
	Crawl(url, 2)

	fmt.Println("Finish!")
	fmt.Println("Time taken:", time.Since(now))

}

func Crawl(url string, depth int) {
	if depth < 0 {
		return
	}

	fmt.Println("[crawl]: searching links")
	urls, err := findLinks(url)
	if err != nil {
		return
	}

	fmt.Println("[crawl]: Mark URL as checked")
	fmt.Printf("Found: %s\n", shortURL(url, 50))
	fetched[url] = true

	fmt.Println("[crawl]: Looping through subURLs")
	for _, u := range urls {
		if !fetched[u] {
			Crawl(u, depth-1)
		}
	}
}
