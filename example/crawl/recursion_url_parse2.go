package crawl

import (
	"fmt"
	"time"
)

type result struct {
	url   string
	urls  []string
	err   error
	depth int
}

func RecursiveURLsConcurrent() {
	fmt.Println("Create map of checked URLs")
	fetched = make(map[string]bool)

	fmt.Println("Set start time")
	now := time.Now()

	url := "http://andcloud.io"
	fmt.Println("Run crawl from root url:", url)
	Crawl2(url, 2)

	fmt.Println("Finish!")
	fmt.Println("Time taken:", time.Since(now))

}

func Crawl2(url string, depth int) {
	fmt.Println("[crawl]: Create channel for results")
	fmt.Print(`
	[crawl]: Create channel for results
	result [
		url   string
		urls  []string
		err   error
		depth int
	]
	`)
	ch := make(chan *result)

	fmt.Println("[crawl]: create func thad send result to channel")
	fetch := func(url string, depth int) {
		fmt.Println("[crawl[fetch]]: Run findLinks")
		urls, err := findLinks(url)

		fmt.Println("[crawl[fetch]]: Send result to chan")
		ch <- &result{url, urls, err, depth}
	}

	fmt.Println("[crawl]: Run fetch goro")
	go fetch(url, depth)
	fetched[url] = true

	fmt.Println("[crawl]: Looping while fetched url are over")
	for fetching := 1; fetching > 0; fetching-- {
		fmt.Println("[crawl]: Read result from channel")
		res := <-ch

		if res.err != nil {
			continue
		}

		fmt.Println("[crawl]: Start process URL")
		fmt.Printf("Found: %s\n", shortURL(res.url, 50))

		if res.depth > 0 {
			for _, u := range res.urls {
				if !fetched[u] {
					fetching++
					go fetch(u, res.depth-1)
					fetched[u] = true
				}
			}
		}
	}
	close(ch)
}
