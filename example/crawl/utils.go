package crawl

import (
	"fmt"
	"net/http"

	"golang.org/x/net/html"
)

var fetched map[string]bool

func shortURL(s string, n int) string {
	i := 0
	for j := range s {
		if i == n {
			return s[:j]
		}
		i++
	}
	return s
}

func findLinks(url string) ([]string, error) {

	fmt.Println("[findLinks]: Getting response from URL", shortURL(url, 50))
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("getting %s: %s", url, resp.Status)
	}

	fmt.Println("[findLinks]: Parsing response Body")
	doc, err := html.Parse(resp.Body)
	resp.Body.Close()

	if err != nil {
		return nil, fmt.Errorf("parsing %s as HTML: %v", url, err)
	}

	fmt.Println("[findLinks]: Run visit and returning results")
	return visit(nil, doc), nil
}

func visit(links []string, n *html.Node) []string {
	if n.Type == html.ElementNode {
		fmt.Println("[visit]: check node for links. Node:", n.Data)
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		for _, a := range n.Attr {
			if a.Key == "href" {
				links = append(links, a.Val)
			}
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		links = visit(links, c)
	}

	return links
}
