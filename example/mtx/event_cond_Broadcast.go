package mtx

import (
	"fmt"
	"sync"
)

func BroadcastExample() {
	fmt.Println("Create shared map and sync.WG")
	var sharedSrc = make(map[string]string)
	var wg sync.WaitGroup

	fmt.Println("Create sync.Mutex and sync.Cond")
	mu := sync.Mutex{}
	c := sync.NewCond(&mu)

	wg.Add(1)
	fmt.Println("Create and run goro which wait for Broadcast")
	go func() {
		fmt.Println("[goro 1]: defer Done()")
		defer wg.Done()

		fmt.Println("[goro 1]: Locking mutex")
		c.L.Lock()
		fmt.Println("[goro 1]: Looping while len < 1")
		for len(sharedSrc) < 1 {
			fmt.Println("[goro 1]: Waiting")
			c.Wait()
		}

		fmt.Println("[goro 1]: Unlock mutex")
		c.L.Unlock()
		fmt.Println("[goro 1]: Printing value from shared map")
		fmt.Println(sharedSrc["rsc1"])
	}()

	wg.Add(1)

	go func() {
		fmt.Println("[goro 2]: defer Done()")
		defer wg.Done()

		fmt.Println("[goro 2]: Locking mutex")
		c.L.Lock()
		fmt.Println("[goro 2]: Looping while len < 2")
		for len(sharedSrc) < 2 {
			fmt.Println("[goro 2]: Waiting")
			c.Wait()

			/*
				c.Wait()
				• suspends execution of the calling goroutine.
				• automatically unlocks c.L
				• Wait cannot return unless awoken by Broadcast or Signal.
				• Wait locks c.L before returning.
				• Because c.L is not locked when Wait first resumes, the
				caller typically cannot assume that the condition is true
				when Wait returns. Instead, the caller should Wait in a
				loop

			*/
		}
		fmt.Println("[goro 2]: Unlock mutex")
		c.L.Unlock()
		fmt.Println("[goro 2]: Printing value from shared map")
		fmt.Println(sharedSrc["rsc2"])
	}()

	fmt.Println("Locking mutex")
	c.L.Lock()
	fmt.Println("Push values to shared map")
	sharedSrc["rsc1"] = "Foo"
	sharedSrc["rsc2"] = "Bar"
	fmt.Println("c.Broadcast()")
	c.Broadcast()
	/*
		c.Broadcast()
		• Broadcast wakes all goroutines waiting on c.
		• It is allowed but not required for the caller to hold c.L during the call.
	*/

	fmt.Println("Unlocking mutex")
	c.L.Unlock()

	wg.Wait()
	fmt.Println("Waiting for goro")
}
