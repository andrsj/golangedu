package mtx

import (
	"fmt"
	"sync"
)

/*
sync.Cond
• Conditional Variable are type
	var c *sync.Cond
• We use constructor method sync.NewCond()
  to create a conditional variable, it takes sync.Locker
  interface as input, which is usually sync.Mutex:

	m := sync.Mutex{}
	c := sync.NewCond(&m)
*/

func SignalExample() {
	fmt.Println("Create var shared map")
	var sharedRsc = make(map[string]string)
	fmt.Println("Create sync.WG")
	var wg sync.WaitGroup

	fmt.Println("Create sync.Mutex and sync.Cond")
	mu := sync.Mutex{}
	c := sync.NewCond(&mu)

	wg.Add(1)
	fmt.Println("Create and run goro which wait for Signal")
	go func() {
		fmt.Println("[goro]: Defer Done() waiting")
		defer func() {
			fmt.Println("[goro]: [defer]: Done goro")
			wg.Done()
		}()

		fmt.Println("[goro]: Locking mutex")
		c.L.Lock()

		fmt.Println("[goro]: Looping while len != 0")
		for len(sharedRsc) == 0 {
			fmt.Println("[goro]: Waiting")
			c.Wait()

			/*
				c.Wait()
				• suspends execution of the calling goroutine.
				• automatically unlocks c.L
				• Wait cannot return unless awoken by Broadcast or Signal.
				• Wait locks c.L before returning.
				• Because c.L is not locked when Wait first resumes, the
				caller typically cannot assume that the condition is true
				when Wait returns. Instead, the caller should Wait in a
				loop

			*/
		}

		fmt.Println("[goro]: Printing value from shared map")
		fmt.Println(sharedRsc["rsc1"])

		fmt.Println("[goro]: Unlocking mutex")
		c.L.Unlock()
	}()

	fmt.Println("Locking mutex")
	c.L.Lock()

	fmt.Println("Push value to shared map")
	sharedRsc["rsc1"] = "Foo"

	fmt.Println("Send signal")
	c.Signal()
	/*
		c.Signal()
		• Signal wakes one goroutine waiting on ¢, if there is any.
		• Signal finds goroutine that has been waiting the longest and notifies that.
		• It is allowed but not required for the caller to hold c.L during the call.
	*/

	fmt.Println("Unlocking mutex")
	c.L.Unlock()

	wg.Wait()
	fmt.Println("Waiting for all goro")
}
