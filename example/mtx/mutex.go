package mtx

import (
	"fmt"
	"sync"
)

func MutexExample() {

	fmt.Println("Init balance[int], wg and mu")
	var balance int
	var wg sync.WaitGroup
	var mu sync.Mutex

	fmt.Println("Init func that add balance")
	deposit := func(amount int) {
		fmt.Println("[deposit]: 🔒 Locking mutex")
		mu.Lock()

		fmt.Println("[deposit]: +balance")
		balance += amount

		fmt.Println("[deposit]: 🔓 Unlocking mutex")
		mu.Unlock()
	}

	fmt.Println("Init func that withdraw balance")
	withdraw := func(amount int) {
		fmt.Println("[withdraw]: 🔒 Locking mutex")
		mu.Lock()

		fmt.Println("[withdraw]: -balance")
		balance -= amount

		fmt.Println("[withdraw]: 🔓 Unlocking mutex")
		mu.Unlock()
	}

	count := 10
	fmt.Println("Add", count, "workers to add balance")
	wg.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			defer wg.Done()
			deposit(1)
		}()
	}

	fmt.Println("Add", count, "workers to withdraw balance")
	wg.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			defer wg.Done()
			withdraw(1)
		}()
	}

	wg.Wait()
	fmt.Println("Getting results")
	fmt.Printf("Deposit: %d\n", balance)
}
