package mtx

import (
	"fmt"
	"sync"
)

func OnceExample() {
	fmt.Println("Init sync.WG and sync.Once")
	var (
		wg   sync.WaitGroup
		once sync.Once
	)

	fmt.Println("Init func that need to be run only ONE time")
	load := func() {
		fmt.Println("Run only once!")
	}

	fmt.Println("Looping")
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			once.Do(load)
		}()
	}

	wg.Wait()
	fmt.Println("The end...")
}
