package channel

import (
	"fmt"
	"time"
)

func TimeoutByChannel() {
	fmt.Println("Create channel [string]")
	ch := make(chan string, 1)

	t := 2 * time.Second
	fmt.Println("Create and run goro with", t, "sleep")
	go func() {
		time.Sleep(t)
		fmt.Println("[goro]: send message to channel [string]")
		ch <- "Message"
	}()

	fmt.Println("Select statement:")
	select {
	case m := <-ch:
		fmt.Println("Read message from channel:")
		fmt.Println(m)
	case <-time.After(3 * time.Second):
		fmt.Println("Timeout!")
	}
}
