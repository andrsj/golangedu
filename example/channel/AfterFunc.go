package channel

import (
	"fmt"
	"math/rand"
	"time"
)

func ClearRaceCondition() {
	fmt.Println("Setup start time value")
	fmt.Println("Start")
	start := time.Now()

	fmt.Println("Create timer")
	var t *time.Timer

	fmt.Println("Create channel for sync between MAIN and sub-goro")
	ch := make(chan struct{})

	fmt.Println("Run func in AfterFunc's goro")
	t = time.AfterFunc(randomDuration(), func() {

		fmt.Println("[goro]: Printing time")
		// fmt.Println(time.Now().Sub(start)) -> rewritten
		fmt.Println(time.Since(start))

		fmt.Println("Send signal into sync channel")
		ch <- struct{}{}
	})

	tm := 5 * time.Second
	fmt.Println("Looping during", tm)
	for time.Since(start) < tm {
		fmt.Println("Wait for signal")
		<-ch

		duration := randomDuration()
		fmt.Println("Reset timer with new duration", duration)
		t.Reset(duration)
	}

	fmt.Println("Sleep main goro", tm)
	time.Sleep(tm)
	fmt.Println("End")
}

// returns random duration between 0 and 1 seconds
func randomDuration() time.Duration {
	return time.Duration(rand.Int63n(1e9))
}
