package channel

import "fmt"

func genMessages(ch chan<- string) {
	fmt.Println("[gen]: Sending msg to first channel")
	ch <- "Message"
}

func relayMessages(chA <-chan string, chB chan<- string) {
	fmt.Println("[relay]: Getting msg from first channel")
	message := <-chA
	fmt.Println("[relay]: Sending msg to second channel")
	chB <- message
}

func DirectedChan() {
	fmt.Println("Create two channels for pipeline")
	ch1 := make(chan string)
	ch2 := make(chan string)

	fmt.Println("Run goro for generating messages")
	go genMessages(ch1)

	fmt.Println("Run goro to relay [redirect] messages")
	go relayMessages(ch1, ch2)

	fmt.Println("Get message from channel in main goro")
	v := <-ch2
	fmt.Println(v)
}
