package channel

import "fmt"

func ChannelOwner() {

	fmt.Println("Create func owner which create send only <-channel [int]")
	owner := func() <-chan int {
		fmt.Println("[owner]: Create channel [int]")
		ch := make(chan int)

		fmt.Println("[owner]: Create and run goro that fill channel")
		go func() {
			fmt.Println("[owner]: [goro]: defer closing channel")
			defer func() {
				fmt.Println("[owner]: [goro]: Closing channel")
				close(ch)
			}()

			fmt.Println("[owner]: [goro]: Loop sending:")
			for i := 0; i < 5; i++ {
				fmt.Println("[owner]: [goro]: Send value:", i, "to channel<- [int]")
				ch <- i
			}
		}()

		fmt.Println("[owner]: Returning receive only <-channel")
		return ch
	}

	fmt.Println("Create func(goro) that read all data from sender <-channel [int]")
	consumer := func(ch <-chan int) {

		fmt.Println("[consumer]: Looping through channel [int]")
		for v := range ch {
			fmt.Println("[consumer]: Read value")
			fmt.Printf("Received: %d\n", v)
		}
		fmt.Println("[consumer]: Done!")
	}

	fmt.Println("Create channel by 'owner' func")
	ch := owner()

	fmt.Println("Start 'consumer' function")
	consumer(ch)
}
