package context

import (
	"context"
	"fmt"
)

type userIdKeyType string
type database map[userIdKeyType]bool

var db database = database{
	"jane": true,
}

func ContextValue() {
	fmt.Println("Create default Cancel context")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	key := "jake"
	fmt.Println("Run function with ctx and key:", key)
	processRequest(ctx, key)
}

func processRequest(ctx context.Context, user_id string) {
	fmt.Println("[processRequest]: Create ctx.WithValue")
	value_ctx := context.WithValue(ctx, userIdKeyType("userIDKey"), userIdKeyType(user_id))

	fmt.Println("[processRequest]: Pass new ctx to next func")
	ch := checkMembership(value_ctx)

	fmt.Println("[processRequest]: Getting results from channel")
	status := <-ch

	fmt.Println("[processRequest]: Printing status")
	fmt.Printf("Membership status of user_id : %s : %v\n", user_id, status)
}

// checkMembership - takes context as input
// extracts the user id information from context
// spins a goroutine to do map lookup
// sends the result on the returned channel
func checkMembership(ctx context.Context) <-chan bool {
	fmt.Println("[checkMembership]: Create chan for sending data")
	ch := make(chan bool)

	fmt.Println("[checkMembership]: Create and run goro")
	go func() {
		fmt.Println("[checkMembership[goro]]: Defer closing chan")
		defer close(ch)

		fmt.Println("[checkMembership[goro]]: Getting value through ctx")
		user_id := ctx.Value(userIdKeyType("userIDKey")).(userIdKeyType)

		fmt.Println("[checkMembership[goro]]: Getting status from map")
		status := db[user_id]

		fmt.Println("[checkMembership[goro]]: Sending status to chan")
		ch <- status
	}()

	fmt.Println("[checkMembership]: Returning channel")
	return ch
}
