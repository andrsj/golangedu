# Context

What is Context Package used for?

* We need a way to propagate request-scoped data
down the call graph.
* We need a way to propagate cancellation signal
down the call graph.
* Context package can be used to send:
  * Request-scoped values
  * Cancellation signals
* Across API boundaries to all goroutines involved in
handling a request.

What is Context Package used for?

* Context package can be used to send:
  * Request-scoped values
  * Cancellation signals
* Across API boundaries to all goroutines involved in
handling a request.

___

## Context deep-dive

```go
// A Context carries a deadline, cancellation signal, and request-scoped values
// across API boundaries. Its methods are safe for simultaneous use by multiple
// goroutines.
type Context interface {

  // Done returns a channel that is closed when this Context is canceled
  // or times out.
  Done() <-chan struct{}
  // Err indicates why this context was canceled, after the Done channel
  // is closed.
  Err() error
  // Deadline returns the time when this Context will be canceled, if any.
  Deadline() (deadline time.Time, ok bool)
  // Value returns the value associated with key or nil if none.
  Value(key interface{}) interface{}
}
```

### Context Package serves two primary purpose

* Provides API’s for cancelling branches of call-graph.
* Provides a data-bag for transporting request-scoped
data through call-graph.

___

## Incoming requests to a server should create a Context

* Create context early in processing task or request.
* Create a top level context

```go
func main() {
  ctx := context.Background()
  // . . .
}
```

* http.Request value already contains a Context.

```go
func handleFunc(w http.ResponseWriter, req *http.Request) {
  ctx, cancel = context.WithCancel(req.Context())
}
```

___

## Outgoing calls to servers should accept a Context

* Higher level calls need to tell lower level calls how long they
are willing to wait.

```go
// Create a context with a timeout of 100 milliseconds
ctx, cancel := context.WithTimeout(req.Context(), 100 * time.Millisecond)
defer cancel()

// Bind the new context into the request
req = req.WithContext(ctx)

// Do will handle the context level timeout
resp, err := http.DefaultClient.Do(req)
```

* `http.DefaultClient.Do()` method to respect cancellation signal
on timer expiry and return with error message.

___

## GO Context idioms

* Pass a Context to function performing I/O
  * Any function that is performing I/O should accept a Context value as it’s first parameter and respect any timeout or deadline configured by the caller.
  * Any API's that takes a Context, the idiom is to have the first parameter accept the Context value.
* Any change to a Context value creates a new Context value that is then propagated forward.
* When a Context is canceled, all Contexts derived from it are also canceled
  * If a parent Context is cancelled, all children derived by that parent Context are cancelled as well.
* Use TODO context if you are unsure about which Context to use
  * If a function is not responsible for creating top level context.
  * We need a temporary top-level Context until we figured out where the actual Context will come from.
* Use context values only for request-scoped data
  * Do not use the Context value to pass data into a function which becomes essential for its successful execution.
  * A function should be able to execute its logic with an empty Context value.

___

## Background

```go
func main() {
  ctx := context.Background()
  // ...
}
```

* Background returns a empty context.  
* Root of any context tree.  
* It is never canceled, has no value and has no deadline.  
* Typically used by `main` function.  
* Acts as a top level context for incoming request.

___

## TODO

```go
func fun() {
ctx := context.TODO()
}
```

* `TODO()` returns an empty Context.
* TODO's intended purpose is to serve as a placeholder.

___

## Context is immutable

* Context package provides function to add new behavior.
* To add cancellation behavior we have function like:
  * `context.WithCancel()`
  * `context.WithTimeout()`
  * `context.WithDeadline()`
* The derived context is passed to child goroutines to facilitate
their cancellation.

___

### WithCancel()

* WithCancel returns a copy of parent with a new Done channel.
* `cancel()` can be used to close context’s done channel.
  * `cancel()` does not wait for the work to stop.
  * `cancel()` may be called by multiple goroutines simultaneously.
  * After the first call, subsequent calls to a `cancel()` do nothing.
* Closing the done channel indicates to an operation to abandon its work and return.
* Canceling the context releases the resources associated with it.

### Workflow Cancel

Parent Goroutine:

```go
ctx, cancel := context.withCancel(context.Background())
ch := generator(ctx)
if n == 5 {
  cancel()

}
```

Child Goroutine:

```go
for {
  select {
  case <-ctx.Done():
    return ctx.Err()
  case dst <- n:
  } 
}
```

___

### WithDeadline()

```go
deadline := time.Now().Add(5 * time.Millisecond)
ctx, cancel := context.WithDeadline(context.Background(), deadline)
defer cancel()
```

* `WithDeadline()` takes parent context and clock time as input.
* `WithDeadline` returns a new Context that __closes its done
channel when the machine’s clock advances past the given
deadline__

> go/src/context/context.go

```go
func WithDeadline(parent Context, d time.Time) (Context, CancelFunc) {
  // ...
  c := &timerCtx{
    cancelCtx: newCancelCtx(parent),
    deadline: d,
  }
  // ...
  c.timer = time.AfterFunc(dur, func() {
    c.cancel(true, DeadlineExceeded) 
  }
  // ...
```

### Workflow Deadline

Child Goroutine

```go
deadline, ok := ctx.Deadline()
if ok {
  if deadline.Sub(time.Now().Add(10 * time.Millisecond)) <= 0 {
    return context.DeadlineExceeded
  }
}
// . . .
for {
  select {
  case <-ctx.Done():
    return ctx.Err()
  case dst <- n:
    n++
    }    
}
```

___

### WithTimeout()

```go
duration := 5 * time.Millisecond
ctx, cancel := context.WithTimeout(context.Background(), duration)
defer cancel()
```

* `WithTimeout()` takes parent context and time duration as input.
* `WithTimeout()` returns a new Context that closes its done channel after the given timeout duration.
* `WithTimeout()` is useful for setting a deadline on the requests to backend servers.

> go/src/context/context.go

```go
func WithTimeout(parent Context, timeout time.Duration) (Context, CancelFunc) {
    return WithDeadline(parent, time.Now().Add(timeout))
  }
)

```

* `WithTimeout()` is a wrapper over `WithDeadline()`

___

### WithValue()

* `WithValue` returns a copy of parent in which the value associated with key is val.
* Use context Values only for request-scoped data that transits processes and APIs, not for passing optional parameters to functions.
* The provided key __must be__ comparable and __should not be of type string or any other built-in type__ to avoid collisions between packages using context. Users of WithValue should define their own types for keys. To avoid allocating when assigning to an `interface{}`, context keys often have concrete type `struct{}`. Alternatively, exported context key variables' static type should be a pointer or interface.
* `ctx.Value` returns the value associated with this context for key, or nil if no value is associated with key. Successive calls to Value with the same key returns the same result.

Parent goroutine:

```go
type IDType string
ctx := context.WithValue(context.Background(), IDType("IDKey"), "<value>")
```

Child goroutine:

```go
user_id := ctx.Value(IDType("IDKey")).(IDType)
```

___

## HTTP Server Timeouts

* Setting timeouts in server is important to conserve system resources and to protect from DDOS attack.
* File descriptors are limited.
* Malicious user can open many client connections, consuming all file descriptors.
* Server will not able to accept any new connection.

`http: Accept error: accept tcp [::]:80: accept: too many open files;
retrying in 1s`

## net/http Timeouts

There are four main timeouts exposed in http.server

* Read Timeout
* Read Header Timeout
* Write Timeout
* Idle Timeout

![Timeout for server](../../img/Timeout.png)

* Read Timeout: covers the time from when the connection is accepted, to when the request body is fully read.
* ReadHeader Timeout: amount of time allowed to read request headers§
* Write Timeout: covers the time from the end of the request header read to the end of the response write.
* Idle Timeout: maximum amount of time to wait for the next request when
keep-alive is enabled.

___

### Set Timeouts by explicitly using a Server

```go
server := &http.Server{
  ReadTimeout:       1 * time.Second,
  ReadHeaderTimeout: 1 * time.Second,
  WriteTimeout:      1 * time.Second,
  IdleTimeout:       30 * time.Second,
  Handler:           serveMux,
}
```

* Set Connection timeouts when dealing with untrusted clients and networks.
* Protect Server from clients which are slow to read and write.

### HTTP Handler Functions

* Connection timeouts apply at network connection level.
* HTTP Handler Functions are unaware of these timeouts, they run to completion, consuming resources.

### http.TimeoutHandler()

> net/http package provides `TimeoutHandler()`

```go
srv := http.Server{
  Addr:         "localhost:8000",
  WriteTimeout: 2 * time.Second, ‘
  Handler:      http.TimeoutHandler(http.HandlerFunc(slowHandler),
                1 * time.Second,
                "Timeout!\n"),
}
```

> `TimeoutHandler` returns a `Handler` that runs input handler with the given time limit.
>> If input handler _runs for longer than its time limit_, the handler sends the client a `503 Service Unavailable error` and HTML error message.

___

### Context Timeouts and Cancellation

* Use Context timeouts and cancellation to propagate the cancellation signal down the call graph.
* The Request type already has a context attached to it.

`ctx := req.Context()`

* Server cancels this context when:
  * Client closes the connection.
  * Timeout

___

[Back to main page](../../README.md)
