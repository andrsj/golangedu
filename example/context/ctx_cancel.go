package context

import (
	"context"
	"fmt"
)

// ContextCancel show hot to use cancel function with
// context, which created by WithCancel method
func ContextCancel() {

	// Simple function that generate values into created chan
	// and return this channel for future work

	fmt.Println("Create generator function")
	generator := func(ctx context.Context) <-chan int {
		fmt.Println("Create channel [int]")
		ch := make(chan int)

		n := 1

		fmt.Println("Create and run goro")
		go func() {

			fmt.Println("[goro]: Run defer for closing channel")
			defer func() {
				fmt.Println("[goro]: [defer]: closing channel")
				close(ch)
			}()

			fmt.Println("[goro]: Looping is select statement")
			for {
				fmt.Println("[goro]: Select statement")
				select {
				case ch <- n:
					fmt.Println("[goro]: Pushing value to chan [int]")
				case <-ctx.Done():
					fmt.Println("[goro]: ctx.Done()")
					return
				}
				fmt.Println("[goro]: incrementing value")
				n++
			}
		}()

		fmt.Println("[goro]: returning channel [int]")
		return ch
	}

	fmt.Println("context.WithCancel")
	ctx, cancel := context.WithCancel(context.Background())

	fmt.Println("Create channel with generator")
	ch := generator(ctx)

	fmt.Println("Looping through channel")
	for n := range ch {
		fmt.Println(n)
		if n == 10 {
			fmt.Println("Canceling goroutine")
			cancel()
		}
	}
	fmt.Println("Canceling goroutine again?")
	cancel()
}
