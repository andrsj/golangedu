package context

import (
	"context"
	"fmt"
	"time"
)

type data struct {
	result string
}

// ContextDeadline shows how to use
// deadline in method WithDeadline
func ContextDeadline() {

	timeDiff := 10 * time.Millisecond
	fmt.Println("Creating deadline with", timeDiff)
	deadline := time.Now().Add(timeDiff)

	fmt.Println("Creating context with deadline")
	ctx, cancel := context.WithDeadline(context.Background(), deadline)

	fmt.Println("Run defer for cancel function")
	defer func() {
		fmt.Println("[defer]: canceling context")
		cancel()
	}()

	fmt.Println("Create function which imitate work")
	compute := func() <-chan data {

		fmt.Println("[compute]: create channel [date{string}]")
		ch := make(chan data)

		fmt.Println("[compute]: create and run goro")
		go func() {

			fmt.Println("[compute]: [goro]: defer closing channel")
			defer close(ch)

			fmt.Println("[compute]: [goro]: create deadline from context")
			deadline, ok := ctx.Deadline()
			if ok {
				fmt.Println("[compute]: [goro]: calculate time difference")
				if deadline.Sub(time.Now().Add(50*time.Millisecond)) < 0 {
					fmt.Println("[compute]: [goro]: deadline checked")
					fmt.Println("Deadline")
					return
				}
			}

			fmt.Println("[compute]: [goro]: 'working'")
			time.Sleep(50 * time.Millisecond)

			fmt.Println("[compute]: [goro]: select statement")
			select {
			case ch <- data{"Done"}:
				fmt.Println("[compute]: [goro]: result sended to channel")
			case <-ctx.Done():
				fmt.Println("[compute]: [goro]: returning from func")
				return
			}
		}()

		fmt.Println("[compute]: returning channel")
		return ch
	}

	fmt.Println("Create channel with 'compute' function")
	ch := compute()

	fmt.Println("Check value from channel")
	d, ok := <-ch
	if ok {
		fmt.Println("Got value 'in-time'")
		fmt.Println("Done:", d)
	} else {
		fmt.Println("NO DATA ^_^")
	}
}
