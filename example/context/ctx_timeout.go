package context

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// ContextTimeout shows how to use
// WithTimeout method in context
func ContextTimeout() {

	fmt.Println("Create request")
	req, err := http.NewRequest("GET", "https://andcloud.io", nil)
	if err != nil {
		log.Fatal("Error:", err)
		return
	}

	timeout := 1000 * time.Millisecond
	fmt.Println("Create timeout:", timeout)
	ctx, cancel := context.WithTimeout(req.Context(), timeout)

	fmt.Println("Defer canceling context")
	defer func() {
		fmt.Println("[defer]: canceling context")
		cancel()
	}()

	fmt.Println("Request with context")
	req = req.WithContext(ctx)

	fmt.Println("Getting response")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal("Error:", err)
	}

	fmt.Println("Defer closing resp.Body")
	defer func() {
		fmt.Println("Closing Body")
		resp.Body.Close()
	}()

	fmt.Println("Copy Body to Stdout")
	nb, err := io.Copy(os.Stdout, resp.Body)
	if err != nil {
		log.Fatal("Error:", err)
	}
	fmt.Println("Written:", nb)
}
